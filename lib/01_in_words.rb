class Integer

  def in_words

    int = self

    result = []

    if self == 0
      result << 'zero'
    end

    [[1000000000000, 'trillion'], [1000000000, 'billion'], [1000000, 'million'], [1000, 'thousand'], [100, 'hundred'],
    [10, ''], [1, '']].each do |num, string|
      next if int < num

      writing = int / num
      int = int % num



      case num
      when 1000000000000
        result << writing.in_words << string
      when 1000000000
        result << writing.in_words << string
      when 1000000
        result << writing.in_words << string
      when 1000
        result << writing.in_words << string
      when 100
        result << ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'][writing] + " #{string}"
      when 10
        if writing == 1 && int != 0
          result << ['eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen'][int - 1]
          break
        else
          result << ['ten','twenty','thirty','forty','fifty','sixty','seventy','eighty','ninety'][writing - 1]
        end
      when 1
        result << ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'][writing]
      end
    end

  result.join(' ')

  end
end
